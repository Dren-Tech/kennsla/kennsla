package infrastructre

import (
	"fmt"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"github.com/labstack/gommon/log"
)

type App struct {
	e *echo.Echo
}

func (a *App) Init() {
	a.e = echo.New()
	a.e.Debug = true

	a.e.Logger.SetLevel(log.DEBUG)
	a.e.Use(middleware.LoggerWithConfig(middleware.LoggerConfig{
		Format: "method=${method}, uri=${uri}, status=${status}, latency: ${latency_human}\n",
	}))

	InitRouter(a.e)
}

func (a *App) Start(host string, port int) {
	a.e.Logger.Info("Start app...")
	a.e.Logger.Fatal(a.e.Start(fmt.Sprintf("%s:%d", host, port)))
}
