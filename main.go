package main

import "kennsla/infrastructre"

func main() {
	app := infrastructre.App{}

	app.Init()

	app.Start("0.0.0.0", 1323)
}
